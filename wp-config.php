<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'alumin_air_amigas' );

/** MySQL database username */
define( 'DB_USER', 'local' );

/** MySQL database password */
define( 'DB_PASSWORD', 'local' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'km~q$M7){(ckSm(u:]9V[T~=1A@rvO5WL0qksrX8fX}nh;>juwJYj}%1As;g*c5C' );
define( 'SECURE_AUTH_KEY',  'HegHHNup]Xjj7bP/A,xW7s]3%Tla&ip{w5h+N5SRYf63?}wsK)OJYakJ>kpxb,u3' );
define( 'LOGGED_IN_KEY',    '$Z8LFj;UdMXCw?TL`48d@*>]LpN)gJJ1Ilkt1fmUx1}rCZhf^Y9|QnlJ/7H>f!jK' );
define( 'NONCE_KEY',        'P,9W{T]8;olg%GFXeICbT0*`Xdy(OPs+7lhcaEt8_@X{F40{kwKtZsJkdV2B~}nf' );
define( 'AUTH_SALT',        '~.|:&YE=eUi@18i1EjUep4`W8+nB;qPn0RAPp.Oy!:GKYprOwW#T3k5<doKSgi@ ' );
define( 'SECURE_AUTH_SALT', 'qAFC5kUUTF/T-<b$JuB+Zy#ka%PA rb:3NBq<=ysvY_D:&FUJQX3RV@s!rVGRpNq' );
define( 'LOGGED_IN_SALT',   ')J%BmU4ROeyA4t:m/;{v3|&FH[Q`40lD3g&Am#%smw``GPK0:vY,wD]$$pP{}[oO' );
define( 'NONCE_SALT',       '5@99>!ezn<{j`PeA:_EsKnaWG J8E$NACJp<]SUm5&$HA) 1Q*1!Z])rat?>6=yx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'amgxy_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

define('WP_ALLOW_MULTISITE', true);

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/airmedia/amigas/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
