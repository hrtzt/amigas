# Amigas.com

### Ruta local

http://localhost/airmedia/amigas

### URL

http://amigas.com/

### Prototipo

https://projects.invisionapp.com/share/6VTJRJGM34D#/screens/379972110

### Especificaciones de diseño


**Fuente**

<link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,700,700i,900&display=swap" rel="stylesheet">

Color:
Morado: #59165b
Lila: #907cb9
Rosa: #ffdeeb
Amarillo: #ffb706

Badge en el footer: 

<a href="https://www.guidestar.org/profile/20-4863071" target="_blank"><img src="https://widgets.guidestar.org/gximage2?o=7038046&l=v4" /></a>