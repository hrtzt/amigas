// Librerías
//@prepros-prepend ../../vendor/fancybox/fancybox.js
//@prepros-prepend ../../vendor/slick/slick.js
//@prepros-prepend_ ../../vendor/tweenmax/TweenMax.min.js
//@prepros-prepend_ ../../vendor/scrollto/ScrollToPlugin.min.js
//@prepros-prepend ../../wpkit/js/site.js

// Components
//@prepros-prepend ../../components/footer/footer.js
//@prepros-prepend ../../components/header/header.js
//@prepros-prepend ../../components/slider/slider.js
//@prepros-prepend ../../components/offcanvas/offcanvas.js
//@prepros-prepend ../../components/imgtext-layout/imgtext-layout.js
//@prepros-prepend ../../components/testimonials/testimonials.js
//@prepros-prepend ../../components/calendar/calendar.js
//@prepros-prepend ../../components/map/map.js
//@prepros-prepend ../../components/newsletter/newsletter.js
//@prepros-prepend ../../components/banner/banner.js

// Modules
//@prepros-prepend ../../modules/_style-guide/style-guide.js
//@prepros-prepend ../../modules/home/home.js
//@prepros-prepend ../../modules/about-us/about-us.js
//@prepros-prepend ../../modules/programs/programs.js
//@prepros-prepend ../../modules/youth-programs/youth-programs.js
//@prepros-prepend ../../modules/get-involved/get-involved.js
//@prepros-prepend ../../modules/blog/blog.js
//@prepros-prepend ../../modules/contact-us/contact-us.js
//@prepros-prepend ../../modules/page/page.js
//@prepros-prepend ../../modules/post/post.js
//@prepros-prepend ../../modules/donate/donate.js


/*
 * Scripts globales
 */

 // Smooth scroll
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
    // On-page links
    if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        &&
        location.hostname == this.hostname
    ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000, function () {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
            });
        }
    }
});