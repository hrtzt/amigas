<?php if( get_option( 'option_private_site' ) ) { if( ! is_user_logged_in() ) { get_template_part( 'wpkit/inc/login' ); return true; } }

/**
*
* Main page
*
* @package WPKit
* @author ALUMIN
* @copyright Copyright (C) Alumin.Agency
* @version WPKIT 3.0
*
*/

get_header();

	if( is_page('home-page') ) {
		get_template_part( 'modules/home/home' );
	} elseif( is_page( 'about-us' ) ) {
		get_template_part( 'modules/about-us/about-us' );
	} elseif( is_page( 'programs' ) ) {
		get_template_part( 'modules/programs/programs' );
	} elseif( is_page( 'youth-programs' ) ) {
		get_template_part( 'modules/youth-programs/youth-programs' );
	} elseif( is_page( 'get-involved' ) ) {
		get_template_part( 'modules/get-involved/get-involved' );
	} elseif( is_page( 'blog' ) || is_home()  ) {
		get_template_part( 'modules/blog/blog' );
	} elseif( is_page( 'contact-us' ) ) {
		get_template_part( 'modules/contact-us/contact-us' );
	} elseif( is_page( 'donate' ) ) {
		get_template_part( 'modules/donate/donate' );
	} elseif( is_page() ) {
		get_template_part( 'modules/page/page' );
	} elseif( is_single() ) {
		get_template_part( 'modules/post/post' );
	} else {
		get_template_part('wpkit/systems/layouts/post');
	}
	
get_footer(); ?>
