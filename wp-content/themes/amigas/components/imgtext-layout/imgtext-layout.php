<div class="imgtext-layout" id="img-text-layout">
    <span class="imgtext-layout_bg-element"></span>
    <div class="imgtext-layout__cols wk-cols">
        <div class="imgtext-layout__col imgtext-layout__col-left wk-col">

            <div class="imgtext-layout__img">
                <?php $imgtxt_layout_img = get_field( 'imgtxt_layout_img' ); ?>
                <img width="545" hegith="418" src="<?= $imgtxt_layout_img['sizes']['large'] ?>" alt="<?= $imgtxt_layout_img['alt'] ?>">
            </div>
        
        </div>
        <div class="imgtext-layout__col imgtext-layout__col-right wk-col">

            <div class="imgtext-layout__content">
                <?php the_field( 'imgtxt_layout_txt' ); ?>
            </div>

        </div>
    </div>
</div>