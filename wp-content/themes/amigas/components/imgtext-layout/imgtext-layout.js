
let imgtxt_container = document.querySelectorAll('.imgtext-layout');
let imgtxt_bg_element = document.querySelectorAll('.imgtext-layout_bg-element');
let screen_width = window.innerWidth;
let i;
for( i = 0; i < imgtxt_container.length; i++ ) {
    imgtxt_container[i].parentElement.parentElement.classList.add('imgtext-layout__parent');
    let imgtext_width = imgtxt_container[i].clientWidth / 4;
    let imgtext_bg_element_width = screen_width / 2 - imgtext_width + 'px';
    if (window.innerWidth > 756) {
        imgtxt_bg_element[i].style.width = imgtext_bg_element_width;
    }
}

window.onresize = () => {

    let imgtxt_container = document.querySelectorAll('.imgtext-layout');
    let imgtxt_bg_element = document.querySelectorAll('.imgtext-layout_bg-element');
    let screen_width = window.innerWidth;
    let i;
    for (i = 0; i < imgtxt_container.length; i++) {
        if( window.innerWidth > 756 ) {
            imgtxt_container[i].parentElement.parentElement.classList.add('imgtext-layout__parent');
            let imgtext_width = imgtxt_container[i].clientWidth / 4;
            let imgtext_bg_element_width = screen_width / 2 - imgtext_width + 'px';
            imgtxt_bg_element[i].style.width = imgtext_bg_element_width;
        } else {
            imgtxt_bg_element[i].style.width = '';
        }
    }

}




