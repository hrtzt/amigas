<h3 class="footer__news-title"><strong>Subscribe</strong> to our newsletter</h3>
<form class="footer__news-form" action="">
    <label for="newsletter-input" class="footer__news-input-icon"></label>
    <input id="newsletter-input" type="text" class="footer__news-input" placeholder="Enter your email">
    <?php //echo do_shortcode( '[contact-form-7 id="387" title="Newsletter"]' ); ?>
</form>