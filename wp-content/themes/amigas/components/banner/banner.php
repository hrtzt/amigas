<div class="banner banner_text-left banner_text-center">
    <img class="banner__bg" src="<?= img( 'banner-youth-programs-1.jpg' ); ?>" alt="Transformando nuestra tierra">
    <div class="banner__content">
        <div class="wk-section-wrap">
            <div>
                <img width="200" src="<?= img( 'logo-tnt.png' ); ?>" alt="TNT Logo" class="banner__img">
                <h3 class="banner__title">Transformando <br>Nuestra Tierra</h3>
            </div>
        </div>
    </div>
</div>