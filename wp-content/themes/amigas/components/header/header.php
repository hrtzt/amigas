<header id="main-header" class="wk-section">
    <div class="wk-section-wrap">

        <div class="main-header__cols wk-cols-me">
            <div class="main-header__col main__header-sets-offcanvas wk-cols-me">

                <div class="wk-col">
                    <a href="<?php bloginfo('url'); ?>" class="main-header__logo">
                        <img width="121" height="81" class="main-header__logo-img" src="<?php echo get_option( 'wk_custom_logo_main' ); ?>" alt="<?php bloginfo( 'name' ); ?>">
                    </a>
                </div>

                <div class="wk-col main__header-sets-offcanvas-col wk-m">
                    <div class="main-header__offcanvas-icon"><span class="fas fa-bars"></span></div>
                </div>

            </div>
            <div class="main-header__col wk-cols wk-d">

                <?php

                    $nav_args = array(
                        'theme_location'  => 'main-nav',
                        'container'       => 'nav',
                        'container_class' => 'main-header__menu',
                        'menu_class'      => 'main-header__menu-list',
                        'menu_id'         => 'main-header__menu-list',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0, // Cuantos niveles de jerarquía se incluirán 0 es todos. -1 imprime todos los niveles en uno mismo.
                        'walker'          => ''
                    );

                    wp_nav_menu( $nav_args );

                ?>

                <ul id="main-header__menu-holder">
                    <li class="menu-item menu-item_no-text menu-item__btn">
                        <a href="<?php bloginfo( 'url' ); ?>/donate" id="btn-donate" class="ui-btn ui-btn_pulse ui-btn_main">
                            <span class="ui-btn__icon ui-btn__icon--left btn-donate__icon fas fa-heart"></span>
                            <span>Donate</span>
                        </a>
                    </li>
                    <li class="menu-item menu-item_no-text menu-item__btn">
                        <a class="fixed-icon fixed-icon_overlap fixed-icon_bg_pink" href="#" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li class="menu-item menu-item_no-text menu-item__btn">
                        <a class="fixed-icon fixed-icon_overlap" href="#" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="menu-item menu-item_no-text menu-item__btn">
                        <a class="fixed-icon fixed-icon_overlap fixed-icon_bg_pink" href="#" target="_blank">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                    <li class="menu-item menu-item_no-text menu-item__btn">
                        <a class="fixed-icon" href="#" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>

                </ul>

            </div>
            
        </div>

    </div>

</header>
