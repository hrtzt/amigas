 <div class="testimonials">
        
    <div class="testimonials__slider">

        <?php 
        
            $args = array(
                'post_type'		=>	'testimonials',
            );
        
            $wp_query = new WP_Query( $args );
        
        ?>
        
        <?php if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
        
            <div class="testimonial">
                <div class="testimonial__title"><?php the_content(); ?></div>
            </div>
            
        <?php endwhile;  wp_reset_query();endif; ?>
        
    </div>
    <div class="testimonials__nav">

        <?php 
        
            $args = array(
                'post_type'		=>	'testimonials',
            );
        
            $wp_query = new WP_Query( $args );
        
        ?>
        
        <?php if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
        
            <div class="testimonials__nav-item">
                <div class="testimonials__nav-content">
                    <span class="testimonials__nav-icon">
                        <?php if( has_post_format( 'video') ) : ?>
                            <img src="<?php img( 'icon-video.png' ); ?>" alt="Comillas">
                        <?php else : ?>
                            <img src="<?php img( 'icon-comillas.png' ); ?>" alt="Comillas">
                        <?php endif; ?>
                    </span>
                    
                    <h3 class="testimonials__nav-title"><?php the_title(); ?></h3>

                </div>
            </div>
            
        <?php endwhile; wp_reset_query(); endif; ?>

    </div>

</div>