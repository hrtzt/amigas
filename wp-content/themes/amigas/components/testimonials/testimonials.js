/*
 * Testimonials
 */

$('.testimonials__slider').slick({
    fade: true,
    rows: false,
    arrows: false,
    asNavFor: '.testimonials__nav',
    autoplay: true
});

$('.testimonials__nav').slick({
    // fade: true,
    rows: false,
    arrows: false,
    slidesToShow: 3,
    asNavFor: '.testimonials__slider',
    centerMode: true,
    focusOnSelect: true,
    infinite: true,
    // variableWidth: true
    responsive: [
        {
            breakpoint: 756,
            settings: {
                slidesToShow: 1,
                dots: true
            }
        },
    ]
});

$('.testimonials__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    let video = slick.$slides[currentSlide].querySelector('video');
    if (video) {
        video.pause();
    }
});
// $('.testimonials__slider').on('afterChange', function (event, slick, currentSlide ) {
//     let video = slick.$slides[currentSlide].querySelector('video');
//     if (video) {
//         video.play();
//     }
// });
