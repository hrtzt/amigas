
<?php 

function calendar_date() {

    ?>

        <div id="date-<?php the_ID(); ?>" class="date" >
            
            <div class="date__formated">

                <?php
                    $formated_date = get_field( 'calendar_date', false, false );
                    $formated_date = new DateTime( $formated_date );
                ?>

                <div class="date__formated-month"><?=$formated_date->format('M');?></div>
                <div class="date__formated-day"><?=$formated_date->format('d');?></div>
            </div>

            <div class="date__content">
                <h1 class="date__title"><?php the_title(); ?></h1>
                <span class="date__day"><?php the_field( 'calendar_date' ); ?></span>
                <address class="date__address">
                    <h6 class="date__address-title">Address:</h6>
                    <span class="date__address-text"><?php the_field( 'calendar_address' ); ?></span>
                </address>
                <div class="date__btns">
                    <a data-fancybox data-options='{ "baseClass" : "fancybox-bg_none" }' data-src="#calendar-form" href="javascript:;" class="ui-btn date__btn ui-btn_lilac ui-btn_pulse date__btn_register">Register</a>
                    <!-- <a data-fancybox data-src="#calendar-form" href="javascript:;" class="ui-btn ui-btn_outline-lilac date__btn date__btn_joinus">Join us</a> -->
                </div>
            </div>

        </div>

    <?php

}

$args = array(
    'post_type'		=>	'calendar',
    'order'         => 'ASC'
);

$wp_query = new WP_Query( $args );

?>

<?php if( $wp_query->have_posts() ) : ?>
        
        <h1 class="calendar__title">Calendar</h1>

        <div id="calendar-nav"></div>

       <?php  $months_slider = array(); ?>

        <?php while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

            <?php 

                $date = get_field('calendar_date', false, false);

                $date = new DateTime($date);

                // print_r($date);

                $months = $date->format('F Y');

                // print_r($date->modify('+1 day'));

                array_push( $months_slider, $months );

                ?>

        <?php endwhile; wp_reset_postdata(); ?>
        
        <?php 

            $months_unique =  array_unique( $months_slider );
        
            // print_r( $months_unique ); 

        ?>

        <div class="calendar">
        
            <?php foreach( $months_unique as $month ) : ?>

                <div class="month">

                    <h3 class="month__title"><?php echo $month; ?></h3>

                    <?php 
                    
                        $args = array(
                            'post_type'		=>	'calendar'
                        );
                    
                        $wp_query = new WP_Query( $args );
                    
                    ?>

                    <div class="dates">

                        <div class="wk-cols">

                            <div class="wk-col">

                                <?php if( $wp_query->have_posts() ) : $contador1 = 0; while( $wp_query->have_posts() ) : $contador1++; $wp_query->the_post(); ?>
                                
                                    <?php if( $contador1 % 2 == 1 ) : ?>
    
                                            <?php calendar_date(); ?>
                                            
                                    <?php endif; ?>
                                
                                <?php endwhile; wp_reset_postdata(); endif; ?>
                                
                            </div>
                            <div class="wk-col">
                                
                                <?php if( $wp_query->have_posts() ) : $contador2 = 0; while( $wp_query->have_posts() ) : $contador2++; $wp_query->the_post(); ?>
                                
                                    <?php if( $contador2 % 2 == 0 ) : ?>
    
                                            <?php calendar_date(); ?>
                                            
                                    <?php endif; ?>
                                
                                <?php endwhile; wp_reset_postdata(); endif; ?>

                            </div>
                            

                        </div>

                    </div>

                </div>

            <?php endforeach; ?>

        </div>
                
<?php endif; ?>

<div id="calendar-form" class="calendar__form">

    <h3 class="calendar__form-title">Register</h3>

    <?php echo do_shortcode( '[contact-form-7 id="230" title="Contact form 1"]' ); ?>

</div>



