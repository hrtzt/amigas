$('.calendar').slick({
    rows: false,
    dots: true,
    appendArrows: '#calendar-nav',
    fade: true,
    nextArrow: '<span class="slick-next slick-arrow icon-arrow icon-arrow_next icon-arrow_lilac"></span>',
    prevArrow: '<span class="slick-prev slick-arrow icon-arrow icon-arrow_prev icon-arrow_lilac"></span>',
});

let calendar_form = document.querySelector('.wpcf7');

if (calendar_form) {
    calendar_form.addEventListener( 'wpcf7submit', function(){
        console.log('Enviado');
    });
}
