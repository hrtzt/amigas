<footer id="main-footer">

    <?php if( !is_page('donate') || !is_page('privacy-policy') ) : ?>

        <section id="footer__news-section" class="footer__news wk-section">
            <div class="wk-section-wrap">
                <?php get_template_part( 'components/newsletter/newsletter' ); ?>
            </div>
        </section>
        
    <?php endif; ?>

    <section id="footer__nav-section" class="footer__nav wk-section">
        <div class="wk-section-wrap">

            <div class="wk-cols">
    
                <?php if( has_nav_menu( 'footer-menu' ) ) : ?>
                    
                    <div class="wk-col">
    
                        <h3 class="footer__nav-title">About Us</h3>
                        <nav class="footer__nav-content">
                            
                            <?php $footer_nav = wp_get_nav_menu_items( 'footer-menu' ); ?>
    
                            <ul class="footer__nav-menu">
                                <?php foreach( $footer_nav as $footer_nav_item ) : ?>
    
                                    <li class="footer__nav-menu-item">
                                        <a class="footer_nav-menu-item-link" href="<?= $footer_nav_item->url ?>"><?= $footer_nav_item->title ?></a>
                                    </li>
                                
                                <?php endforeach; ?>
                            </ul>
    
                        </nav>
                    
                        
                    </div>
                
                <?php endif; ?>
    
                <div class="wk-col">
    
                    <h3 class="footer__nav-title">Contact Us</h3>
                    <div class="footer__nav-content">
    
                        <address itemscope itemtype="http://schema.org/Organization">
                            <address class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                <p class="address__location">
                                    <strong>Address:</strong>
                                    <span itemprop="streetAddress">
                                        <span itemprop="streetAddress"> 1059 Tierra del Rey <br> Suite F Chula Vista</span>
                                        <span itemprop="addressRegion">CA</span> 
                                        <span itemprop="postalCode">91910</span>
                                    </span>
                                </p>
                                <p class="address__phone">
                                    <strong>Phone:</strong>
                                    <span itemprop="telephone">(619) 427-0301</span>
                                </p>
                                <p class="address__mail">
                                    <strong>Mail:</strong>
                                    <a  href="mailto:info@amigaspuntocom.org" itemprop="email">info@amigaspuntocom.org</a>
                                </p>
                                <p class="address__social-links">
                                    <a class="fixed-icon fixed-icon_color_blue fixed-icon_overlap" href="http://fb.me/usuario"><i class="fab fa-facebook"></i></a>
                                    <a class="fixed-icon fixed-icon_color_blue fixed-icon_overlap" href="http://twitter.com/usuario"><i class="fab fa-twitter"></i></a>
                                    <a class="fixed-icon fixed-icon_color_blue fixed-icon_overlap" href="http://yotube.com/usuario"><i class="fab fa-youtube"></i></a>
                                    <a class="fixed-icon fixed-icon_color_blue fixed-icon_overlap" href="http://linkedin.com/usuario"><i class="fab fa-linkedin"></i></a>
                                </p>
                            </address>
                        </address>
    
                    </div>
                    
                </div>
    
                <div class="wk-col">
                    
                    <h3 class="footer__nav-title">Support Us</h3>
                    <div class="footer__nav-content">
                        <a href="#" class="footer__nav-btn-donate ui-btn ui-btn_pink ui-btn_pulse">
                            <span class="ui-btn__icon ui-btn__icon--left btn-donate__icon fas fa-heart"></span>
                            <span>Donate</span
                        ></a>
                    </div>
    
                </div>
    
                <div class="wk-col">
    
                    <div class="footer-nav__content">
                        <img src="<?= get_template_directory_uri() ?>/assets/img/seal-of-transparency.png" alt="Seal of transparency">
                    </div>
    
                </div>
    
            </div>

        </div>
    </section>

    <section id="footer__legal-section" class="footer__legal wk-section">
        <div class="wk-section-wrap">
            <div class="wk-cols">
                <div class="footer__legal-col footer__legal-col_left wk-col">
                    <?php if( get_privacy_policy_url() ) : ?>
                        <a href="<?= get_privacy_policy_url() ?>">Privacy Policy</a>
                    <?php endif; ?>
                </div>
                <div class="footer__legal-col footer__legal-col_right wk-col">
                    <span>amigaspuntocom.org&copy; 501 (C)3 All rights reserved</span>
                </div>
            </div>
        </div>
    </section>

</footer>