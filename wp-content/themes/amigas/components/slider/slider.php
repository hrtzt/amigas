<div class="slider">

    <?php if( have_rows( 'slider' ) ) : while( have_rows( 'slider' ) ) : the_row(); ?>
    
        <div class="slide <?= $border_style = get_sub_field('slide_border_style') ? 'slide_border-style' : ''; ?>">

            <?php $slide_image = get_sub_field( 'slide_image' ); ?>

            <img class="slide__img" src="<?= $slide_image['sizes']['large'] ?>" alt="<?= $slide_image['alt'] ?>">

            <?php if(  get_sub_field('slider_add_text') ) : ?>
                <div class="slide__content slide__content_<?= get_sub_field( 'slide_content_pos' ) ?>">
                    <?php the_sub_field( 'slide_content' ); ?>
                </div>
            <?php endif; ?>

        </div>
    
    <?php endwhile; endif; ?>

</div>