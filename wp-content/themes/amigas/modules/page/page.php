<div id="section-content" class="wk-section">
    <div class="wk-section-wrap">

        <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

            <article>
                
                <h1 class="ui-title-big"><?php the_title(); ?></h1>
    
                <?php the_content(); ?>

            </article>

        
        <?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>

    </div>
</div>