$('.slide-set').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    // centerMode: true,
    variableWidth: true,
    prevArrow: '<span class="slider__arrow slider__arrow_prev icon-arrow icon-arrow_main icon-arrow_prev"></span>',
    nextArrow: '<span class="slider__arrow slider__arrow_next icon-arrow icon-arrow_main icon-arrow_next"></span>'
});

