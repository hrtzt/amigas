<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <section id="section-programs-slider" class="wk-section">
        <?php get_template_part( 'components/slider/slider' ); ?>
    </section>

    <section id="section-tnt-info" class="wk-section">
        <div class="wk-section-wrap">

            <article class="article">

                <div class="article__row-single-paragraph article_paragraph-big article_purple">
                    <?php the_content(); ?>
                </div>

            </article>

        </div>
    </section>

    <section id="section-programs-bullets" class="wk-section">
        <div class="wk-section-wrap">

            <div class="wk-cols">

                <?php 
                
                    function programs_bullet( $icon, $text) { 

                        ?>

                        <div class="wk-col">
                        
                            <div class="programs-bullet">
                                <div class="programs-bullet__icon">
                                    <img src="<?= $icon ?>" alt="Icon people" width="100" height="">
                                </div>
                                <p class="programs-bullet__content"><?= $text ?> </p>
                            </div>
            
                        </div>

                        <?php 
                    }

                ?>

                <?php 

                    $page_id = get_queried_object_id();

                ?>

                <?php if( have_rows( 'programs_bullets' ) ) : while( have_rows( 'programs_bullets' ) ) : the_row(); ?>
                
                    <?php programs_bullet( get_sub_field('programs_bullet_icon'), get_sub_field('programs_bullets_content')); ?>
                
                <?php endwhile; endif; ?>

            </div>

        </div>
    </section>

    <section id="programs-video" class="wk-section">
        <div class="wk-section-wrap">

            <div class="video__container video_frame">
                
                <?php the_field( 'programs_video' ); ?>
                
            </div>

        </div>
    </section>

    <section id="section-youth-programs-slideset" class="wk-section">
        <div class="wk-section-wrap">

            <div class="slide-set">

                <?php $gallery = get_field( 'programs_slideset' ); ?>

                <?php foreach($gallery as $image) : ?>

                <?php print_r($image); ?>

                    <div>
                        <img src="<?= $image['url'] ?>'" alt="TNT group 1">
                    </div>

                <?php endforeach; ?>
            </div>

        </div>
    </section>

    <section id="section-youth-programs-join-tnt" class="wk-section">
        <div class="wk-section-wrap">

            <article class="article">
                <div class="article__row article__row-single-paragraph article_paragraph-big article_purple">

                    <div>
                        <?php the_field( 'programs_jointnt' ); ?>
                    </div>

                </div>
            </article>

        </div>
        
    </section>


    <section id="section-youth-programs-logos" class="wk-section">
        <div class="wk-section-wrap">

            <div class="youth-programs__logos">

                <?php $logos_gallery = get_field( 'programs_logos' ); ?>

                <?php foreach( $logos_gallery as $logo ) : ?>
                    <span><img class="youth-programs__logo" src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>" class="youth-programs__logo"></span>
                <?php endforeach; ?>
            </div>

        </div>
    </section>

    <section id="section-testimonials" class="wk-section">

        <?php get_template_part( 'components/testimonials/testimonials' ); ?>

    </section>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>
