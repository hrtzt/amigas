<section id="section-blog-feed" class="wk-section">
    <div class="wk-section-wrap">


        <?php 

            $posts_per_page = 9;

            $args = array(
                'post_type'		=>	'post',
                'posts_per_page'=> $posts_per_page
            );

            $wp_query = new WP_Query( $args );

        ?>

        <?php if( $wp_query->have_posts() ) : $contador = 0; while( $wp_query->have_posts() ) : $contador++; $wp_query->the_post(); ?>

            <?php if( $contador == 1 ) : ?>
                <div class="posts__container">
            <?php endif; ?>

            <div <?php post_class(); ?>>
                <div class="post__thumbnail">
                    <img class="" src="<?php the_post_thumbnail_url( 'thumbnail' ); ?>" alt="<?php the_title(); ?>">
                    <?php if( has_post_format( 'gallery' ) ) :?>
                        <span class="post__format-icon">
                            <i class="fas fa-plus"></i>
                        </span>
                    <?php elseif( has_post_format( 'video' ) ) :?>
                        <span class="post__format-icon">
                            <i class="fas fa-play"></i>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="post__content">
                    <span class="post__date"><?php echo get_the_date( 'M j' ); ?></span>
                    <h1 class="post__title"><?php the_title(); ?></h1>
                    <div class="post__excerpt"><?php the_excerpt(); ?></div>
                    <a href="<?php the_permalink(); ?>" class="post__read-more-link">Read all</a>
                </div>
            </div>

            <?php if( $contador % 3 == 0  && $contador < $posts_per_page)  : ?>

                </div><!--.post-->
                <div class="posts__container">

            <?php endif; ?>

        <?php endwhile; endif; ?>

    </div>
</section>
