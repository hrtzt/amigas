/*
 * Click en la sección de about
 */

$('.about-tabs__title').on('click', function(){
    
    if ($('.about-tabs__title').not(this).hasClass('about-tabs__current-tab') ) {
        $('.about-tabs__title').removeClass('about-tabs__current-tab');
    }
    $(this).addClass('about-tabs__current-tab');

});


/*
 * CLick en la sección de about lo trae a Top
 */

let section_about_tabs = document.getElementById('section-about-tabs');
if (section_about_tabs ) {
    section_about_tabs.addEventListener('click', function() {
        section_about_tabs.scrollIntoView({
            behavior: 'smooth'
        });
    });

}