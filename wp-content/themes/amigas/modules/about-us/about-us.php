<section id="section-slider-about-us" class="wk-section">
    <?php get_template_part( 'components/slider/slider' ); ?>
</section>


<section id="section-about-tabs" class="wk-section">
    <div class="wk-section-wrap">

        <div class="wk-cols">
            <div class="wk-cols">
                
                <a href="#section-who-we-are" class="about-tabs__title about-tabs__current-tab">About</a>

            </div>
            <div class="wk-cols">
                
                <a href="#letter-from-the-president" class="about-tabs__title">Letter form the president</a>

            </div>
            <div class="wk-cols">
                
                <a href="#section-staff" class="about-tabs__title">Board of Directos and Staff</a>

            </div>
        </div>

    </div>
</section>

<section id="section-who-we-are" class="wk-section">
    <div class="wk-section-wrap">

        <article class="article-about-us article">

            <div class="article__row article__row-single-paragraph">

                <div>
                    <h1 class="article__title">WHO WE ARE</h1>
            
                    <p>Amigas Punto Com (APC) is a ... Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea molestias doloremque rerum aliquid sapiente alias veniam quam magni suscipit minus ialias veniam quam magni suscipit minus ialias veniam quam magni suscipit minus impedit corrupti error, nihil omnis ipsa aperiam minima, dolorum libero!</p>
                </div>
                

            </div>

            <div class="article__row">

                <div class="article__col article__col-left">

                    <h1 class="article__title">What we do</h1>
                    
                    <p>We empower and equipo Ltino women... Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos illo ad temporibus quae, quo ipsum ullam distinctio perspiciatis, aut atque aperiam aliquid, accusantium in sequi quos voluptatibus dolore. Veniam, iure.ut atque aperiam aliquid, accusantium in sequi quos voluptatibus dolore. Veniam, iure.</p>
                    
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate enim illo dolorem laboriosam perspiciatis repellendus iure similique delectus doloribus ex cum at obcaecati, quos quaerat vitae, reprehenderit, labore atque. Dolor.</p>
                    
                    <h1 class="article__title">Mission</h1>

                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto beatae aperiam excepturi itaque vero dignissimos magni molestias in nam, eligendi quos maxime labore fuga? Tempora harum voluptatum illum sapiente consectetur!</p>

                </div>

                <div class="article__col article__col-right">
                    <img src="<?= img('about-what-we-do.png') ?>" alt="What we do">
                </div>

            </div>
            
            <div class="article__row article__row_invert-in-mobile">

                <div class="article__col article__col-left">
                    <img src="<?= img('about-letter-from-the-president.png') ?>" alt="What we do">
                </div>

                <div id="letter-from-the-president" class="article__col article__col-right">

                    <h1 class="article__title">Letter from the President</h1>
                    
                    <p>We empower and equipo Ltino women... Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos illo ad temporibus quae, quo ipsum ullam distinctio perspiciatis, aut atque aperiam aliquid, accusantium in sequi quos voluptatibus dolore. Veniam, iure.ut atque aperiam aliquid, accusantium in sequi quos voluptatibus dolore. Veniam, iure.</p>
                    
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate enim illo dolorem laboriosam perspiciatis repellendus iure similique delectus doloribus ex cum at obcaecati, quos quaerat vitae, reprehenderit, labore atque. Dolor.</p>
                    
                    <h1 class="article__title">Mission</h1>

                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto beatae aperiam excepturi itaque vero dignissimos magni molestias in nam, eligendi quos maxime labore fuga? Tempora harum voluptatum illum sapiente consectetur!</p>

                    <p class="article__p_center-on-mobile">
                        <span class="article__subtitle">Vivian Nieto</span>
                        <span class="article__label">President</span>
                    </p>

                </div>


            </div>
            

        </article>

        

    </div>
</section>


<section id="section-staff" class="wk-section">
    <div class="wk-section-wrap">

        <?php 
        
            $args = array(
                'post_type'		=>	'staff',
                'posts_per_page'=> -1,
                'order'         => 'ASC'
            );
        
            $wp_query = new WP_Query( $args );
        
        ?>

        <div class="staff-members staff-members_directors">

            <h3 class="staff-members__title">Our Board of Directors</h3>
            
            <?php if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                <?php if( get_field( 'staff_meber_section' ) == true )  : ?>

                    <div class="staff-member staff-member_<?php echo strtolower(str_replace( ' ', '-', get_field( 'staff_member_position' ))); ?>">
                        <img class="staff-member__avatar" width="140" height="140" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <h1 class="staff-member__title"><?php the_title(); ?></h1>
                        <span class="staff-member__position"><?php the_field( 'staff_member_position' ); ?></span>
                        <span class="staff-member__email"><?php the_field( 'staff_member_email' ); ?></span>
                        <a href="mailto:<?php the_field( 'staff_member_email' ); ?>?subject=Im contacting from <?php bloginfo('name'); ?>&body=Dear <?php the_title(); ?>: 
                        " class="staff-member__contact-btn">Contact me</a>
                    </div>

                <?php endif; ?>            
            
            <?php endwhile; wp_reset_postdata(); endif; ?>

        </div>

        <div class="staff-members staff-members_staff">

            <h3 class="staff-members__title">Our Staff</h3>

            <?php if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
    
                <?php if( !get_field( 'staff_meber_section' ) == true )  : ?>
    
                    <div class="staff-member staff-member_<?php echo strtolower(str_replace( ' ', '-', get_field( 'staff_member_position' ))); ?>">
                        <img class="staff-member__avatar" width="140" height="140" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <h1 class="staff-member__title"><?php the_title(); ?></h1>
                        <span class="staff-member__position"><?php the_field( 'staff_member_position' ); ?></span>
                        <span class="staff-member__email"><?php the_field( 'staff_member_email' ); ?></span>
                        <a href="mailto:<?php the_field( 'staff_member_email' ); ?>?subject=Im contacting from <?php bloginfo('name'); ?>&body=Dear <?php the_title(); ?>: 
                        " class="staff-member__contact-btn">Contact me</a>
                    </div>
    
                <?php endif; ?>            
            
            <?php endwhile; wp_reset_postdata(); endif; ?>

        </div>

        

    </div>
</section>