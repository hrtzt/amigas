<section id="contact-us-staff" class="wk-section">
    <div class="wk-section-wrap">
    
        <h1 class="contact-us__main-header-title">Contact us</h1>

        <div class="contact-us__areas">

            <?php $area_termns = get_terms( 'area' ); ?>

            <?php foreach( $area_termns as $area_term ) : ?>

                <div class="contact-us__area">

                    <div class="contact-us__header">
                        <img class="contact-us__area-logo" src="<?php echo get_field( 'area_logo', 'term_' . $area_term->term_id ); ?>" alt="<?php echo $area_term->name; ?>">
                        <h1 class="contact-us__area-title"><?php echo $area_term->name; ?><br></h1>
                    </div>

                    <hr>
                    
                    <?php 
                    
                    $args = array(
                        'post_type'		=>	'staff',
                        'posts_per_page'=> -1,
                        'order'         => 'ASC',
                        'tax_query'     => array(
                            array(
                                'taxonomy'  => 'area',
                                'field'     => 'slug',
                                'terms'     => $area_term->slug
                            ),
                        ),
                    );
                
                    $wp_query = new WP_Query( $args );
                    
                    if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                        <div class="contact-us__member">
                            <img class="contact-us__member-avatar" width="90" height="90" src="<?php the_post_thumbnail_url( 'thumbnail' ); ?>" alt="<?php the_title(); ?>">
                            <div class="contact-us__member-info">
                                <h1 class="contact-us__member-name"><?php the_title(); ?></h1>
                                <?php if( ! has_term( 'community-outreach-media', 'area' ) && ! has_term( 'donations-sponsors-fundraising', 'area' )  ) : ?>
                                    <h2 class="contact-us__member-position"><?php the_field( 'staff_member_position' ); ?></h2>
                                <?php endif; ?>
                                <h3 class="contact-us__member-email"><?php the_field( 'staff_member_email' ); ?></h2>
                            </div>
                        </div>


                    <?php endwhile; wp_reset_query(); endif; ?>

                </div>

            <?php endforeach; ?>

        </div>

    
    </div>
</section>

<section id="section-contact-data" class="wk-section">
    <div class="wk-section-wrap">

        <div class="contact__data">

            <?php if( have_rows( 'contact_us_icons' ) ) : while( have_rows( 'contact_us_icons' ) ) : the_row(); ?>
            
                <div class="contact__data-item">
                
                        <img class="contact__data-icon" src="<?= get_sub_field('contact_us_icon') ?> " alt="Icon">
                        <div class="contact__data-text"><?= get_sub_field('contact_us_content') ?></div>

                </div>
            
            <?php endwhile; endif; ?>

        </div>

    </div>
</section>

<section id="section-map" class="wk-section">

    <div id="map"></div>

    <script>
    
        function initMap() {

            // Color base del mapa
            var colorBase = '#494949';
            // Url base con la ubicación del marcador

            // baseUrl se añadió via wp_localize_script para hacer disponible la url del sitio
            // como objeto en java script
            // var iconBase = window.baseUrl.homeUrl + '/wp-content/themes/amigas/assets/img/theme/icons/';
            // var icons = {
            //     marker: {
            //         icon: iconBase + 'icon_maps_marker.png'
            //     }
            // };
            var features = [
                {
                    position: new google.maps.LatLng(<?= the_field('contact_us_map_lat'); ?>, <?= the_field('contact_us_map_long'); ?>),
                    type: 'marker'
                }
            ];
            for (var i = 0; i < features.length; i++) {
                var marker = new google.maps.Marker
            }
            var sitelocation = { lat: <?= the_field('contact_us_map_lat'); ?>, lng: <?= the_field('contact_us_map_long'); ?> };
            var styledMapType = new google.maps.StyledMapType(
                [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": colorBase
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9a9999"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#212121"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#FF7400"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": colorBase
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#1b1b1b"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#3e3e3e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9a9999"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#373737"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#464646"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway.controlled_access",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#4e4e4e"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#c3c3c3"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#c3c3c3"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#403f3f"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#3d3d3d"
                            }
                        ]
                    }
                ],
                { name: 'Styled Map' });

            // Create a map object, and include the MapTypeId to add
            // to the map type control.

            var map = new google.maps.Map(document.getElementById('map'), {
                center: sitelocation,
                zoom: 16,
                mapTypeControlOptions: {
                    mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                        'styled_map']
                }
            });
            var marker = new google.maps.Marker({
                position: sitelocation,
                icon: icons[features[0].type].icon,
                map: map
            });
            // var marker = new google.maps.Marker({position: lat: 19.2523217, lng: -99.0968079, map: map});

            //Associate the styled map with the MapTypeId and set it to display.
            map.mapTypes.set('styled_map', styledMapType);
            map.setMapTypeId('styled_map');
        }

    </script>

</section>

<section id="section-contact-form" class="wk-section">
    <div class="wk-section-wrap">

        <?php echo do_shortcode( '[contact-form-7 id="231" title="Contact form 1_copy"]' ); ?>    

    </div>
</section>