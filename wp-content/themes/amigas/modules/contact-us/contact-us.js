let contact_form = document.querySelector('#section-contact-form .wpcf7');

if( contact_form ) {
    contact_form.addEventListener('wpcf7submit', function() {
        // contact_form.remove();
        let form_fields = contact_form.querySelectorAll('p');
        for( var ff = 0; ff < form_fields.length; ff++ ) {
            form_fields[ff].remove();
        }
    });
}