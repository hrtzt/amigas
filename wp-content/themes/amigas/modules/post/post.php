<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <section id="section-post-header" class="wk-section">
        <div class="wk-section-wrap">

            <div class="post__header">
                <img class="post__header-img" src="<?php the_post_thumbnail_url('large'); ?>" alt="<?php the_title(); ?>">
            </div>
            
        </div>
    </section>

    <section id="section-post-nav" class="wk-section">
        <div class="wk-section-wrap">

            <div class="post__nav">

                <div class="post__nav-link post__nav-prev-link">
                    <?php previous_post_link('<span class="post__nav-icon icon-arrow icon-arrow_lilac icon-arrow_prev"></span> %link', 'Anterior'); ?> 
                </div>

                <div class="post__nav-link post__nav-next-link">
                    <?php next_post_link('%link <span class="post__nav-icon icon-arrow icon-arrow_lilac icon-arrow_next"></span>', 'Siguiente'); ?> 
                </div>

            </div>

        </div>
    </section>

    <article id="section-article" class="wk-section post">
        <div class="wk-section-wrap">

            <?php edit_post_link( '<span class="fas fa-pencil-alt"></span>' ); ?>
    
            <span class="post__date"><?php the_date( 'M j' ); ?></span>
            <h1 class="post__title_big"><?php the_title(); ?></h1>
            <?php the_content(); ?>
    
        </div>
    </article>


<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>
