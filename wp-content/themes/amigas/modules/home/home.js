

/*
 *    Animación de números en la sección we care of people
 *
 * 
*/

if (document.getElementById('section-we-care-of-people')) {
    window.addEventListener('scroll', numInView );
}


function numInView()  {
    let window_size = window.innerHeight;
    let posicionY = window.scrollY;
    let we_care_sec = document.getElementById('section-we-care-of-people');
    let we_care_sec_position = we_care_sec.getBoundingClientRect();
    if (posicionY >= we_care_sec_position.bottom) {
        $('.we-care__fact-num').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        window.removeEventListener('scroll', numInView);
        let we_care_fact_container = document.querySelectorAll('.we-care__fact-num-container');
        let i;
        for (i = 0; i <= we_care_fact_container.length; i++ ) {
            we_care_fact_container[i].style.opacity = '1';
        }
    }
}
