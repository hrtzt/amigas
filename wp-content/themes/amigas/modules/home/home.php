<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <section id="section-slider-home" class="wk-section">
        <?php get_template_part( 'components/slider/slider' ); ?>
    </section>

    <section id="section-imgtext-1" class="wk-section">
        <div class="wk-section-wrap">
            <?php get_template_part( 'components/imgtext-layout/imgtext-layout' ); ?>
        </div>
    </section>

    <section id="section-programs-info" class="wk-section">
        <div class="wk-section-wrap">

            <div class="programs-info">
                <div class="programs-info__cols wk-cols">
                    <div class="programs-info__col-left wk-col">

                        <?php if( have_rows( 'home_programs' ) ) : $contador = 0; while( have_rows( 'home_programs' ) ) : $contador++; the_row(); ?>

                            <?php if( $contador % 2 == 1 ) : ?>
                                <img class="programs-info__img" src="<?php the_sub_field( 'home_program_img' ); ?>" alt="Programs">
                                <div class="programs-info__content">
                                    <h3 class="programs-info__title">
                                        <i class="programs-info__icon fas fa-heart"></i>
                                        <span><?php the_sub_field( 'home_program_title' ); ?></span>
                                    </h3>
                                    <p class="programs-info__p"><?php the_sub_field( 'home_program_content' ); ?></p>
                                </div>
                            <?php endif; ?>
                        
                        <?php endwhile; endif; ?>
    
    
                    </div>
                    <div class="programs-info__col-right wk-col">
    
                        <?php if( have_rows( 'home_programs' ) ) : $contador = 0; while( have_rows( 'home_programs' ) ) : $contador++; the_row(); ?>

                            <?php if( $contador % 2 == 0 ) : ?>
                                <img class="programs-info__img" src="<?php the_sub_field( 'home_program_img' ); ?>" alt="Programs">
                                <div class="programs-info__content">
                                    <h3 class="programs-info__title">
                                        <i class="programs-info__icon fas fa-heart"></i>
                                        <span><?php the_sub_field( 'home_program_title' ); ?></span>
                                    </h3>
                                    <p class="programs-info__p"><?php the_sub_field( 'home_program_content' ); ?></p>
                                </div>
                            <?php endif; ?>
                        
                        <?php endwhile; endif; ?>
    
                    </div>
                </div>
            </div>


        </div>
    </section>

    <section id="section-video-home" class="wk-section">

        <a class="video-home" class="fancybox"data-options='{ "baseClass" : "fancybox-bg_solid fancybox-bg_lilac" }'  data-fancybox data-src="https://www.youtube.com/watch?v=<?php the_field('home_video'); ?>?autoplay=1" href="javascript:;">
            <img class="video-home__thumbnail-img" src="<?= img( 'background-video.jpg' ); ?>" alt="" class="video-homve__thumbnail">
            <img class="video-home__icon" src="<?= img( 'icon-video-white.png' ) ?>" alt="Play video">
            <h1 class="video-home__title"><?php the_field( 'home_video_title' ); ?></h1>

        </a>

    </section>

    <section id="section-we-care-of-people" class="wk-section">
        <div class="wk-section-wrap">
        
            <h3 class="we-care__title">We care of people</h3>
            <div class="we-care__cols wk-cols">
                <?php
                        
                    function facts($numero, $simbolo, $titulo) {

                        echo '
                            <div class="we-care__col">
                
                                <span class="we-care__fact">
                                    <span class="we-care__fact-num-container">
                                        <span class="we-care__fact-num">' . $numero . '</span>
                                        <span class="we-care__fact-symbol">' . $simbolo . '</span>
                                    </span>
                                    <span class="we-care__fact-title">' . $titulo . '</span>
                                </span>

                            </div>
                        ';

                    }

                    if( have_rows( 'home_facts' ) ) : while( have_rows( 'home_facts' ) ) : the_row();
                    
                        facts( get_sub_field('home_fact_number'), get_sub_field('home_fact_symbol'), get_sub_field('home_fact_title') );
                        
                    endwhile; endif; 

                ?>

               
            </div>

        </div>
    </section>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>