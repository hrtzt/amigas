<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <section id="section-donate" class="wk-section">
        <div class="wk-section-wrap">
            
            <div class="donate">
                <header class="donate__header">
                    <h1 class="donate__title">
                        <?php the_title(); ?>
                    </h1>
                    <span class="donate__icon"><i class="fas fa-heart"></i></span>
                </header>
                <div class="donate__number">
                    <span class="donate__number-symbol">$</span>
                    <span class="donate__number-quantity"><input class="donate__number-quantity-input" type="number" value="0"></span>
                </div>
                <a href="" class="ui-btn ui-btn_lilac donate__btn">Donate with paypal</a>
                <p class="ui-leyend-text">Amigas Punto Com does not collect credit card information. Your donation is processed directly through PayPal in its secure system. Your donation is Tax Deductible.</p>
            </div>
            
        </div>
    </section>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>

    

