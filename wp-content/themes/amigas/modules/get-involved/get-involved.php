<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <section id="section-get-involved-slider" class="wk-section">
        <?php get_template_part( 'components/slider/slider' ); ?>
        <div class="banner__leyend wk-section-wrap">
            <p class="ui-leyend-text">Amigas Punto Com does not collect credit card information. Your donation is processed directly through PayPal in its secure system. Your donation is Tax Deductible.</p>
        </div>
    </section>

    <section id="section-imgtext-2" class="wk-section">
        <div class="wk-section-wrap">
            <?php get_template_part( 'components/imgtext-layout/imgtext-layout' ); ?>
        </div>
    </section>

    <section id="section-thank-you-por-your-support" class="wk-section">
        <div class="wk-section-wrap">

            <article class="article">
                <div class="article__row article__row-single-paragraph article_paragraph-big article_purple">

                    <div>
                        <?php the_content(); ?>
                    </div>

                </div>
            </article>

            <hr>
            
        </div>
    </section>


    <section id="section-get-involved-logos" class="wk-section">
        <div class="wk-section-wrap">

            <div class="get-involved__logos">
                <?php foreach( get_field('get_involved_logos') as $logo ) : ?>
        
                    <img class="get-involved__logo" src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
        
                <?php endforeach; ?>
            </div>

        </div>
    </section>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>
