<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

    <section id="section-programs-slider" class="wk-section">
        <?php get_template_part( 'components/slider/slider' ); ?>
    </section>

    <section id="section-programs" class="wk-section">
        <div class="wk-section-wrap">

            <article class="article">

                <div class="article__row article__row-single-paragraph article_paragraph-big">

                    <div>
                        <?php the_content(); ?>
                    </div>

                </div>
            </article>

        </div>
    </section>

    <section id="section-programs-bullets" class="wk-section">
        <div class="wk-section-wrap">

            <div class="wk-cols">

                <?php 
                
                    function programs_bullet( $icon, $text) { 

                        ?>

                        <div class="wk-col">
                        
                            <div class="programs-bullet">
                                <div class="programs-bullet__icon">
                                    <img acf src="<?php echo $icon; ?>" alt="Icon people" width="100" height="">
                                </div>
                                <p class="programs-bullet__content"><?php echo $text; ?> </p>
                            </div>
            
                        </div>

                        <?php 
                    }

                ?>

                <?php if( have_rows( 'programs_bullets' ) ) : while( have_rows( 'programs_bullets' ) ) : the_row(); ?>
                
                    <?php programs_bullet( get_sub_field('programs_bullet_icon'), get_sub_field('programs_bullets_content')); ?>
                
                <?php endwhile; endif; ?>
            
            </div>

        </div>
    </section>

    <section id="section-topics" class="wk-section">
        <div class="wk-section-wrap">

            <article class="article">

                <div class="article__row article__row-single-paragraph">

                    <div class="topics-header">
                        <h1 class="topics__title">Topics</h1>
                        <p>We offer ongoing weekly Workshops, Individual Mentoring, Seminars and Conferences.</p>
                    </div>

                </div>

                <div class="topics-feed">

                    <?php if( have_rows( 'topics' ) ) : while( have_rows( 'topics' ) ) : the_row(); ?>
                    
                        <?php $topic_img = get_sub_field( 'topic_image' ); ?>
                        
                        <div class="topics-item">
                            <img src="<?= $topic_img['sizes']['medium'] ?>" alt="<?=  $topic_img['title'] ?>" class="topics-item__img">
                            <h1 class="topics-item__title"><?php the_sub_field( 'topic_title' ); ?></h1>
                            <p class="topics-item__content"><?php the_sub_field( 'topic_content' ); ?></p>
                        </div>
                        
                    <?php endwhile; endif; ?>
                    
                </div>

            </article>

        </div>
    </section>

    <section id="section-calendar" class="wk-section">
        <div class="wk-section-wrap">
        
            <?php get_template_part( 'components/calendar/calendar' ); ?>

        </div>
    </section>

    <section id="section-testimonials" class="wk-section">

        <?php get_template_part( 'components/testimonials/testimonials' ); ?>

    </section>

<?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>
