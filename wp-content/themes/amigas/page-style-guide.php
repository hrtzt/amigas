<?php if( get_option( 'option_private_site' ) ) { if( ! is_user_logged_in() ) { get_template_part( 'wpkit/inc/login' ); return true; } }

/**
*
* Styleguide 
*
* @see https://github.com/google/code-prettify
*
* @package WPKit
* @author ALUMIN
* @copyright Copyright (C) Alumin.Agency
* @version WPKIT 3.0
*
*/

get_header(); 

    if(is_user_logged_in()) {
        
        get_template_part( 'modules/_style-guide/style-guide' );
    
    }

get_footer(); ?>
