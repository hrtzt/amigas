	<!DOCTYPE html>


	<html lang="<?php bloginfo('language'); ?>" dir="<?php bloginfo('text_direction'); ?>" <?php wk_schema_global_type(); ?><?php body_class('wk-wrap-1280 '); ?>">

		<head <?php wk_opengraph_header(); ?>>

			<?php wp_head(); ?>

		</head>

		<body>

			<div id="wrapper">

				<?php get_template_part( 'components/header/header' ); ?>

				<?php get_template_part( 'components/offcanvas/offcanvas' ); ?>

				
