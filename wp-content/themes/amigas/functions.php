<?php
/*
*
*  Contiene las funciones implementadas en el template.
*
* @package WPKit
* @author ALUMIN
* @version WPKIT 2.0
*/

/*******************************************************************************
WPKit */

	include_once( get_stylesheet_directory() . '/wpkit/config.php' );


/***************************************************************************
* Página de opciones ACF */

	if( function_exists('acf_add_options_page') ) {

		// acf_add_options_page(array(
		// 	'page_title' 	=> 'Testimonials',
		// 	'menu_title'	=> 'Testimonials',
		// 	'menu_slug' 	=> 'testimonials',
		// 	'capability'	=> 'edit_posts',
		// 	'redirect'		=> false,
		// 	'icon_url'		=> 'dashicons-marker',
		// 	'position'		=> '4',
		// ));

		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Herramientas',
		// 	'menu_title'	=> 'Herramientas',
		// 	'parent_slug'	=> 'opciones-de-panel',
		// ));

		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Opciones',
		// 	'menu_title'	=> 'Opciones',
		// 	'parent_slug'	=> 'options-wpkit',
		// ));

	}

/* ***********************************************************************************************************************
* Custom post type template */

	// 'Custom_post_type_name'

		if ( ! function_exists('wk_custom_post_type') ) {

			// Register Custom Post Type
			function wk_custom_post_type() {

				$ctp_name = 'staff';
				$name = 'Staff';
				$names = 'Members';

				$labels = array(
					'name'                  => _x( $names, 'Post Type General Name', 'wpkit_text_domain' ),
					'singular_name'         => _x( $name, 'Post Type Singular Name', 'wpkit_text_domain' ),
					'menu_name'             => __( 'Board ' . $names, 'wpkit_text_domain' ),
					'name_admin_bar'        => __( $names, 'wpkit_text_domain' ),
					'archives'              => __( $names, 'wpkit_text_domain' ),
					'attributes'            => __( $names . ' Attributes', 'text_domain' ),
				);
				$rewrite = array(
					'slug'                  => $ctp_name,
					'with_front'            => true,
					'pages'                 => true,
					'feeds'                 => true,
				);
				$args = array(
					'label'                 => __( $name, 'wpkit_text_domain' ),
					'description'           => __( 'Publicaciones en el sitio', 'wpkit_text_domain' ),
					'labels'                => $labels,
					'supports'              => array(
													'title',
													'thumbnail',
													'custom-fields',
												),
					'hierarchical'          => true,
					'public'                => true,
					'show_ui'               => true,
					'menu_position'         => 5,
					'menu_icon'             => 'dashicons-shield',
					'show_in_menu'          => true,
					'show_in_admin_bar'     => true,
					'show_in_nav_menus'     => true,
					'can_export'            => true,
					'has_archive'           => true,
					'exclude_from_search'   => false,
					'publicly_queryable'    => true,
					'rewrite'               => $rewrite,
					// 'capabilities'          => $capabilities,
					'show_in_rest'			=> true,
				);
				register_post_type( $ctp_name, $args );

			}
			add_action( 'init', 'wk_custom_post_type', 0 );
		}

		function custom_taxonomy() {

			$labels = array(
				'name'                       => _x( 'Area', 'Taxonomy General Name', 'text_domain' ),
				'singular_name'              => _x( 'Area', 'Taxonomy Singular Name', 'text_domain' ),
				'menu_name'                  => __( 'Area', 'text_domain' ),
			);
			$args = array(
				'labels'                     => $labels,
				'hierarchical'               => true,
				'public'                     => true,
				'show_ui'                    => true,
				'show_admin_column'          => true,
				'show_in_nav_menus'          => true,
				'show_tagcloud'              => true,
			);
			register_taxonomy( 'area', array( 'staff' ), $args );

		}
		add_action( 'init', 'custom_taxonomy', 0 );

		// 'Custom_post_type_ Testimonials'

		if ( ! function_exists('wk_custom_post_type_testimonials') ) {

			// Register Custom Post Type
			function wk_custom_post_type_testimonials() {

				$ctp_name = 'testimonials';
				$name = 'Testimonial';
				$names = 'Testimonials';

				$labels = array(
					'name'                  => _x( $names, 'Post Type General Name', 'wpkit_text_domain' ),
					'singular_name'         => _x( $name, 'Post Type Singular Name', 'wpkit_text_domain' ),
					'menu_name'             => __( $names, 'wpkit_text_domain' ),
					'name_admin_bar'        => __( $names, 'wpkit_text_domain' ),
					'archives'              => __( $names, 'wpkit_text_domain' ),
					'attributes'            => __( $names . ' Attributes', 'text_domain' ),
				);
				$rewrite = array(
					'slug'                  => $ctp_name,
					'with_front'            => true,
					'pages'                 => true,
					'feeds'                 => true,
				);
				$args = array(
					'label'                 => __( $name, 'wpkit_text_domain' ),
					'description'           => __( 'Publicaciones en el sitio', 'wpkit_text_domain' ),
					'labels'                => $labels,
					'supports'              => array(
													'title',
													'editor',
													'thumbnail',
													'custom-fields',
												),
					'hierarchical'          => true,
					'public'                => true,
					'show_ui'               => true,
					'menu_position'         => 5,
					'menu_icon'             => 'dashicons-star-filled',
					'show_in_menu'          => true,
					'show_in_admin_bar'     => true,
					'show_in_nav_menus'     => true,
					'can_export'            => true,
					'has_archive'           => true,
					'exclude_from_search'   => false,
					'publicly_queryable'    => true,
					'rewrite'               => $rewrite,
					// 'capabilities'          => $capabilities,
					'show_in_rest'			=> true,
				);
				register_post_type( $ctp_name, $args );

			}
			add_action( 'init', 'wk_custom_post_type_testimonials', 0 );
		}
		if ( ! function_exists('wk_custom_post_type_calendar') ) {

			// Register Custom Post Type
			function wk_custom_post_type_calendar() {

				$ctp_name = 'calendar';
				$name = 'Calendar';
				$names = 'Calendar';

				$labels = array(
					'name'                  => _x( $names, 'Post Type General Name', 'wpkit_text_domain' ),
					'singular_name'         => _x( $name, 'Post Type Singular Name', 'wpkit_text_domain' ),
					'menu_name'             => __( $names, 'wpkit_text_domain' ),
					'name_admin_bar'        => __( $names, 'wpkit_text_domain' ),
					'archives'              => __( $names, 'wpkit_text_domain' ),
					'attributes'            => __( $names . ' Attributes', 'text_domain' ),
				);
				$rewrite = array(
					'slug'                  => $ctp_name,
					'with_front'            => true,
					'pages'                 => true,
					'feeds'                 => true,
				);
				$args = array(
					'label'                 => __( $name, 'wpkit_text_domain' ),
					'description'           => __( 'Publicaciones en el sitio', 'wpkit_text_domain' ),
					'labels'                => $labels,
					'supports'              => array(
													'title',
													// 'editor',
													// 'thumbnail',
													// 'custom-fields',
												),
					'hierarchical'          => true,
					'public'                => true,
					'show_ui'               => true,
					'menu_position'         => 5,
					'menu_icon'             => 'dashicons-calendar-alt',
					'show_in_menu'          => true,
					'show_in_admin_bar'     => true,
					'show_in_nav_menus'     => true,
					'can_export'            => true,
					'has_archive'           => true,
					'exclude_from_search'   => false,
					'publicly_queryable'    => true,
					'rewrite'               => $rewrite,
					// 'capabilities'          => $capabilities,
					'show_in_rest'			=> true,
				);
				register_post_type( $ctp_name, $args );

			}
			add_action( 'init', 'wk_custom_post_type_calendar', 0 );
		}

/*******************************************************************************
Tus funciones */

/*********************************************************************************
 * img()
 * 
 * Función a modo de shortcut para obtener ruta de imágenes
 * 
 * $img:string nombre de la imagen
 */

function img($img) {

	echo get_template_directory_uri() . '/assets/img/' . $img;

}

/*********************************************************************************
 * Register scripts y estílos
 */

function amgs_register_scripts() {

	wp_register_style( 'nunito-g', 'https://fonts.googleapis.com/css?family=Nunito:400,400i,700,700i,900&display=swap' );
	wp_enqueue_style( 'nunito-g' );

}

add_action( 'wp_head', 'amgs_register_scripts', 1 );



/*********************************************************************************
 * Post formats
 * 
 * Función que declara soporte para formatos de post
 * 
 * @see https://wordpress.org/support/article/post-formats/
 * 
 * Revisa si el post tiene un formato asignado has_post_format()
 * 
 */

function amgs_post_formats() {

	add_theme_support( 'post-formats', array( 'video' ) );
	add_post_type_support( 'testimonials', 'post-formats' );

}

add_action('after_setup_theme', 'amgs_post_formats');


/*********************************************************************************
 * Google maps api
 */


// Añade baseUrl como objeto en java script, contiene la url de wordpress
// window.baseUrl.homeUrl
function wp_url_a_js() {

	wp_localize_script( 'scripts', 'baseUrl', array( 'homeUrl' => esc_url(home_url()) ) );

}

add_action( 'wp_enqueue_scripts', 'wp_url_a_js' );

// Añade los atributos async y defer para el script de google maps
add_filter('script_loader_tag', 'scripts_adicionales_async_attr', 10, 2);
function scripts_adicionales_async_attr($tag, $handle) {
    if ( 'maps-api' !== $handle )
        return $tag;
    return str_replace( ' src', ' async defer src', $tag );
}


// Incluye la api de google maps en el footer
add_action( 'wp_enqueue_scripts', 'scripts_adicionales', 100 );
function scripts_adicionales() {
    wp_register_script( 'maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDR4G6aYaUllrNrJTQsl9t0o-nswjobqwg &callback=initMap','' ,'', true  ); 
    wp_enqueue_script('maps-api');
}


/*******************************************************************************
Shortcodes de botónes */

function amgs_shortcode_button_donate( $atts, $content = null) {

	// $a = shortcode_atts( array(
	// 	'content'	=> 'Button'
	// ) , $atts );

	ob_start(); 
	
		?>

			<a href="<?= get_bloginfo(); ?>/donate" id="btn-donate" class="ui-btn ui-btn_big ui-btn_pulse ui-btn_main">
				<span class="ui-btn__icon ui-btn__icon--left btn-donate__icon fas fa-heart"></span>
				<span><?= $content ?></span>
			</a>

		<?php

		return ob_get_clean();
}

add_shortcode( 'donate_button', 'amgs_shortcode_button_donate' );

function amgs_shortcode_button( $atts, $content = null) {

	$a = shortcode_atts( array(
		'link'	=> '',
		'color'	=> 'main',
	) , $atts );

	ob_start(); 
	
		?>

			<p>
				<a href="<?= $a['link'] ?>" class="ui-btn ui-btn_<?= $a['color'] ?>">
					<?= $content ?>
				</a>
			</p>

		<?php

		return ob_get_clean();
}

add_shortcode( 'button', 'amgs_shortcode_button' );


function amgs_shortcode_button_join_tnt( $atts, $content = null) {

	$a = shortcode_atts( array(
		'color'	=> 'main',
	) , $atts );

	ob_start(); 
	
		?>

			<a data-fancybox data-src="#join-tntform" href="javascript:;" class="ui-btn ui-btn_<?= $a['color'] ?>"><?= $content ?></a>

			<div id="join-tntform" class="calendar__form">
			
				<h3 class="calendar__form-title">Register</h3>
			
				<?php echo do_shortcode( '[contact-form-7 id="230" title="Contact form 1"]' ); ?>
			
			</div>
		<?php

		return ob_get_clean();
}

add_shortcode( 'button_join_tnt', 'amgs_shortcode_button_join_tnt' );


